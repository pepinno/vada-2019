<?php
// $conn
require_once("connect.php");
try {
// 3. Query
    $stmt = $conn->prepare("SELECT kraj_kod,nazev FROM kraj");
    $stmt->execute();

// 4. Fetch
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    echo "<ol>";
    foreach ($stmt->fetchAll() as $value) {
        echo "<li>
                <a href='okres.php?kraj={$value['kraj_kod']}'>
                {$value['nazev']}
                </a>
              </li>";
    }
    echo "</ol>";
} catch (PDOException $e) {
    echo "Query failed: " . $e->getMessage();
}
// 5. Close
$conn = null;