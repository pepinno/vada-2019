<?php
session_start();
require_once("connect2.php");

$dataOK = false;
if (isset($_POST['user']) && filter_var($_POST['user'], FILTER_VALIDATE_EMAIL)) {
    if (isset($_POST['pass']) && !empty($_POST['pass'])) {
        $dataOK = true;
    }
}

if ($dataOK) {
    $stmt = $conn->prepare("SELECT heslo FROM uzivatele WHERE email=?");
    $stmt->bind_param('s', $_POST['user']);
    $stmt->execute();
    $stmt->bind_result($hash);
    $stmt->fetch();
    if (password_verify($_POST['pass'], $hash)) {
        $_SESSION['user'] = $_POST['user'];
        echo '<a href="tajna.php">Teď můžeš jít na tajnou stránku</a>';

    } else header("Location: login.html");
}
$conn->close();