<?php
require_once("connect2.php");
$dataOK = false;
if(isset($_POST['user']) && filter_var($_POST['user'], FILTER_VALIDATE_EMAIL)) {
    if (isset($_POST['pass']) && isset($_POST['pass2'])) {
        if (!empty($_POST['pass'])) {
            if ($_POST['pass'] == $_POST['pass2']) {
                if (isset($_POST['url']) && empty($_POST['url'])) {
                    // Tady je vše ok, mohu zapsat do DB
                    $dataOK = true;
                } // je to robot
            } // hesla nejsou shodná
        } // heslo je prázdné
} // hesla nepřišla
} // email nepřišel, nebo není email

if($dataOK) {
    $stmt = $conn->prepare("INSERT INTO uzivatele VALUES (?, ?)");
    $stmt->bind_param('ss', $_POST['user'], $hash);
    $hash = password_hash($_POST['pass'], PASSWORD_DEFAULT);
    if ($stmt->execute() === TRUE) {
        echo "Uživatel zaregistrován";
    } else {
        if ($conn->errno == 1062) echo 'Uživatel je již registrován';
        else echo "Error #". $conn->errno ." : " . $conn->error;
    }
}
else header("Location: registrace.html");
$conn->close();
