<?php

/**
 * Created by PhpStorm.
 * User: student
 * Date: 5.4.2019
 * Time: 13:11
 */
class User
{
    public $email;

    function __construct($email)
    {
        $this->email = $email;
    }
    function __toString() {
        return "Uživatel: ".$this->email;
    }
    function __destruct() {
        echo "Uživatel smazán.";
    }

    private function overHeslo($heslo)
    {
        try {
            $db = "vada";
            $conn = PDOconnect($db);
            $stmt = $conn->prepare("SELECT heslo FROM p_users
                                WHERE email = :email)");
            $stmt->bindParam(':email', $this->email);
            $stmt->execute();

            $result = $stmt->fetch();
            $hash =  $result["heslo"];

            return password_verify ( $heslo , $hash );

        } catch (PDOException $e) {
            echo "Insert failed: " . $e->getCode();
            return false;
        }
    }


    function login($heslo)
    {
            if ($this->overHeslo($heslo)) {
                $_SESSION['pp_user'] = $this->email;
                $_SESSION['pp_ip'] = $_SERVER['REMOTE_ADDR'];
                echo "Uživatel {$this->email} je génius";
            }
            else echo "Špatné heslo";
    }

    function registruj($heslo)
    {
        try {
            $conn = PDOconnect("vada");
            $stmt = $conn->prepare("INSERT INTO p_users (email, heslo)
                                VALUES (:email, :hash)");
            $hash = password_hash($heslo, PASSWORD_DEFAULT);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':hash', $hash);
            $stmt->execute();
            echo "Uživatel {$this->email} byl zaregistrován";
            $conn = null;
        } catch (PDOException $e) {
            if ($e->getCode() == 23000) echo "Uživatel existuje.";
            else echo "Insert failed: " . $e->getCode();
        }
    }

    function zmenHeslo($heslo_old, $heslo_new)
    {
        try {
            if ($this->overHeslo($heslo_old)) {
                $conn = PDOconnect("vada");
                $stmt = $conn->prepare("UPDATE p_users
                                        SET heslo=:hash
                                        WHERE email=:email");
                $stmt->bindParam(':email', $this->email);
                $stmt->bindParam(':hash', $hash);

                $hash = password_hash($heslo_new, PASSWORD_DEFAULT);

                $stmt->execute();
                echo "Heslo bylo nastaveno";
                $conn = null;
            }

        } catch (PDOException $e) {
            echo "Kouzlo se nepovedlo: " . $e->getCode();
        }
    }




}