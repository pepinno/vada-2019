<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 5.4.2019
 * Time: 13:17
 */
require_once "../PDO/connect.php";
require_once "User.php";

session_start();

if (isset($_POST)) {
    if (isset($_POST['user']) &&
        filter_var($_POST['user'], FILTER_VALIDATE_EMAIL)
    ) {
        $pepa = new User($_POST['user']);

        if (isset($_POST['pass'])) {
            if (isset($_POST['pass2']) &&
            $_POST['pass'] == $_POST['pass2']
        ) {
                $pepa->registruj($_POST['pass']);
            } else $pepa->login($_POST['pass']);
        }
    }
}



echo $pepa;
unset($pepa);