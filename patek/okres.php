<?php

// Connect to DB (var $conn)
require_once("connect.php");
// Prepare and bind
$stmt = $conn->prepare("SELECT nazev FROM okres WHERE kraj_kod=?");
$stmt->bind_param("i", $_GET['kraj']);
$stmt->bind_result($row);
$stmt->execute();

// Fetch to Array
include("okres.tmp.php");

// Disconnect from DB
require_once("disconnect.php");