<?php
// set variable $conn
require_once("connect.php");
// Create query
$sql = "SELECT kraj_kod, nazev FROM kraj";

// Send to DB server
$result = $conn->query($sql);

// Fetch to Array
echo "<ol>";
while($row = $result->fetch_assoc()) {
    echo "<li>
            <a href='okres.php?kraj={$row['kraj_kod']}'>
            {$row['nazev']}
            </a>
          </li>";
}
echo "</ol>";
// unset variable $conn
require_once("disconnect.php");
