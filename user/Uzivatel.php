<?php

/**
 * Created by PhpStorm.
 * User: student
 * Date: 13.12.2018
 * Time: 10:59
 */
class Uzivatel
{
    public $email;
    protected $heslo;

    function __construct($email)
    {
        $this->email = $email;
    }

    function __toString() {
        return "Uživatel: ".$this->email;
    }

    function registrace($heslo)
    {
        global $conn;
        try {
            $hash = password_hash($heslo, PASSWORD_DEFAULT);
            $stmt = $conn->prepare("INSERT INTO uzivatel2 (email, heslo) VALUES (:e, :h)");
            $stmt->bindParam(":e", $this->email);
            $stmt->bindParam(":h", $hash);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            if ($e->getCode() == 23000) echo "Uživatel je již registrován";
            else echo "Chyba registrace: "  .$e->getMessage();
            return false;
        }
    }

    function login($heslo)
    {
        global $conn;
        try {
            // overi heslo v DB a vrati true nebo false;
            $stmt = $conn->prepare("SELECT email, heslo FROM uzivatel2 WHERE email=:adr");
            $stmt->bindParam(':adr', $this->email);
            $stmt->execute();
            if ($stmt->rowCount() === 1) { // uzivatel existuje v DB
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                if (password_verify($heslo, $user['heslo'])) {
                    return true;           // Heslo sedí
                }
            }
            return false;
        } catch (PDOException $e) {
            echo "Chyba loginu: " . $e->getMessage();
            return false;
        }
    }

}