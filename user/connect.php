<?php
require_once("settings.php");
// $conn = mysqli_connect($server, $user, $pass, $db) or die("Nepovedlo se.");
try {
    $conn = new PDO("mysql:host=$server;dbname=$db", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    die("Nepovedlo se: " . $e->getMessage());
}
