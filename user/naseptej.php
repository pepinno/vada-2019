<?php
try {
    if (isset($_GET['zacatek']) && strlen($_GET['zacatek']) > 2) {
        $conn = new PDO("mysql:host=localhost;dbname=uiradr", "vada", "zkouska");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT nazev FROM obec WHERE nazev LIKE :z");
        $like = $_GET['zacatek'] . "%";
        $stmt->bindParam(':z', $like);
        $stmt->execute();
        if ($stmt->rowCount() == 0) echo "Nic nenalezeno";
        else {
            $res = $stmt->fetchAll();
            foreach ($res as $o) {
                echo $o["nazev"] . "<br>";
            }
        }
    }
} catch (PDOException $e) {
    echo "Chyba obcí";
}
$conn = null;
