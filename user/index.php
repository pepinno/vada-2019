<?php
require_once("header.php");

if (isset($_POST['akce'])) {
    if ($_POST['akce'] == 'registrace') {
        try {
            $stmt = $conn->prepare("INSERT INTO user (email, pass) VALUES (:email, :pass)");
            $stmt->bindParam(':email', $_POST['email']);
            $stmt->bindParam(':pass', $password_hash);

            $password_hash = password_hash($_POST['pass'], PASSWORD_DEFAULT);

            $stmt->execute();

            echo "Uživatel s emailem: ".$_POST['email']. " byl zaregistrován";

        } catch (PDOException $e) {
            echo "Registrace se nezdařila: " . $e->getMessage();
        }


    }
}


include("registrationForm.html");

include_once("footer.php");
