<?php
require_once("connect.php");

try {
// 3. Query
$stmt = $conn->prepare("SELECT nazev FROM okres WHERE kraj_kod=:k");
$stmt->bindParam(":k", $_GET['kraj']);
$stmt->execute();

// 4. Fetch
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    echo "<ol>";
    foreach ($stmt->fetchAll() as $value) {
        echo "<li>{$value['nazev']}</li>";
    }
    echo "</ol>";
} catch (PDOException $e) {
    echo "Query failed: " . $e->getMessage();
}
// 5. Close
$conn = null;