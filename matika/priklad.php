<?php session_start() ?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Příklad</title>
</head>
<body>
<?php
define("NEJMIN", 0);
define("NEJVIC", 100);
function vygenerujDveCisla($od, $do) {
    $GLOBALS['a'] = rand($od, $do);
    $GLOBALS['b'] = rand($od, $do);
}
vygenerujDveCisla(NEJMIN,NEJVIC);

if(!isset($_SESSION['skore'])) $_SESSION['skore'] = 0;

if (isset($_POST['vysledek'])) {
    $a = $_POST['a'];
    $b = $_POST['b'];
    if($_POST['vysledek'] != "") {
        if($_POST['vysledek'] == $_POST['a'] + $_POST['b'] ) {
            $_SESSION['skore'] ++;
            vygenerujDveCisla(NEJMIN,NEJVIC);
        }
        else $_SESSION['skore']--;
    }
    else echo "Nepřišlo nic, ale to nevadí";
}
?>
<form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
    <?= $a ?> + <?= $b ?> =
    <input type="text" name="vysledek" autoco       mplete="off">
    <input type="hidden" name="a" value="<?= $a ?>">
    <input type="hidden" name="b" value="<?= $b ?>">
    <input type="submit" value="Zkontrolovat">
</form>
<div style="font-size: 2em; font-weight: bold">
    Skóre: <?= $_SESSION['skore'] ?>
    </div>
</body>
</html>